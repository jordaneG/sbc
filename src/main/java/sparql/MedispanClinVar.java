package sparql;

import model.Instance;
import model.Pair;
import org.apache.log4j.Logger;
import org.openrdf.OpenRDFException;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPTupleQuery;

import java.util.ArrayList;
import java.util.List;


public class MedispanClinVar extends Action {

    private static final Logger LOGGER = Logger.getLogger(MedispanClinVar.class);
    private List<Instance> matrix;

    public MedispanClinVar() {
        super();
        this.matrix = new ArrayList<>();
    }

    public List<Instance> getMatrix() {
        return matrix;
    }

    @Override
    public void execute(Pair pair) throws OpenRDFException {
        HTTPTupleQuery query = getQuery(getConnection(), pair);
        query.setMaxQueryTime(600000);

        TupleQueryResult result = query.evaluate();
        while (result.hasNext()) {
            BindingSet bindingSet = result.next();
            String[] gene_attributeCplt = (bindingSet.getValue("clinvar_Gene")).toString().split("/");
            String[] phenotypeCplt = bindingSet.getValue("mediSpan_Disease").toString().split("/");
            String[] drug_attributeCplt = bindingSet.getValue("atc_class").toString().split("/");
            String[] gd_linkCplt = (bindingSet.getValue("drug_Action").toString()).split("/");
            String[] gp_linkCplt = bindingSet.getValue("clinvar_Variant").toString().split("/");
            String[] dp_linkCplt = bindingSet.getValue("medispan").toString().split("/");
            Instance instance = new Instance(pair.getGene() + "_" + pair.getDrug(), gene_attributeCplt[gene_attributeCplt.length - 1], phenotypeCplt[phenotypeCplt.length - 1], drug_attributeCplt[drug_attributeCplt.length - 1], gd_linkCplt[gd_linkCplt.length - 1], gp_linkCplt[gp_linkCplt.length - 1], dp_linkCplt[dp_linkCplt.length - 1], pair.getAssociation());

            matrix.add(instance);
        }
        result.close();
    }

    private HTTPTupleQuery getQuery(RepositoryConnection connection, Pair pair) throws MalformedQueryException, RepositoryException {
        return (HTTPTupleQuery) connection.prepareTupleQuery(QueryLanguage.SPARQL,
                "PREFIX atc: <http://bio2rdf.org/atc:>\n" +
                        "PREFIX bio2rdfv: <http://bio2rdf.org/bio2rdf_vocabulary:>\n" +
                        "PREFIX clinvar: <http://bio2rdf.org/clinvar:>\n" +
                        "PREFIX clinvarv: <http://bio2rdf.org/clinvar_vocabulary:>\n" +
                        "PREFIX dbv: <http://bio2rdf.org/drugbank_vocabulary:>\n" +
                        "PREFIX disgenet: <http://rdf.disgenet.org/resource/gda/>\n" +
                        "PREFIX drugbank: <http://bio2rdf.org/drugbank:>\n" +
                        "PREFIX mapping: <http://biodb.jp/mappings/>\n" +
                        "PREFIX medispan: <http://orpailleur.fr/medispan/>\n" +
                        "PREFIX ncbigene: <http://bio2rdf.org/ncbigene:>\n" +
                        "PREFIX pharmgkb: <http://bio2rdf.org/pharmgkb:>\n" +
                        "PREFIX pharmgkbv: <http://bio2rdf.org/pharmgkb_vocabulary:>\n" +
                        "PREFIX pubchemcompound: <http://bio2rdf.org/pubchem.compound:>\n" +
                        "PREFIX sider: <http://bio2rdf.org/sider:>\n" +
                        "PREFIX siderv: <http://bio2rdf.org/sider_vocabulary:>\n" +
                        "PREFIX sio: <http://semanticscience.org/resource/>\n" +
                        "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                        "PREFIX so: <http://bio2rdf.org/sequence_ontology:>\n" +
                        "PREFIX umls: <http://bio2rdf.org/umls:>\n" +
                        "PREFIX uniprot: <http://bio2rdf.org/uniprot:>\n" +
                        "\n" +
                        "\n" +
                        "SELECT DISTINCT ?clinvar_Gene ?clinvar_Variant ?mediSpan_Disease ?medispan ?atc_class ?drug_Action\n" +
                        "WHERE {\n" +
                        "\t#GD Link\n" +
                        "\tpharmgkb:" + pair.getGene() + " pharmgkbv:x-uniprot ?uniProt_Gene.\n" +
                        "\t?drugBank_Gene dbv:x-uniprot ?uniProt_Gene.\n" +
                        "\t?relation ?p ?drugBank_Gene.\n" +
                        "\t\n" +
                        "\t?relation dbv:drug ?drugBank_Drug.\n" +
                        "\n" +
                        "\tOPTIONAL {pharmgkb:" + pair.getDrug() + " pharmgkbv:x-drugbank ?drugBank_Drug.}\n" +
                        "\t\n" +
                        "\tOPTIONAL {pharmgkb:" + pair.getDrug() + " pharmgkbv:x-pubchemcompound ?pubChem_Compound.\n" +
                        "\t\t?drugBank_Drug dbv:x-pubmedchemcompound ?PubChem_Compound.}\n" +
                        "\n" +
                        "\tOPTIONAL {pharmgkb:" + pair.getDrug() + " pharmgkbv:x-pubchemcompound ?pubChem_Compound.\n" +
                        "\t\t\t?drugBank_Drug dbv:x-pubchemcompound ?pubChem_Compound.}\n" +
                        "\n" +
                        "\t?drugBank_Drug dbv:x-atc ?atc_class.\n" +
                        "\n" +
                        "\t?relation dbv:action ?drug_Action.\n" +
                        "\n" +
                        "\t##DP Link\n" +
                        "\t#MediSpan\n" +
                        "\tOPTIONAL{pharmgkb:" + pair.getDrug() + " pharmgkbv:x-umls ?mediSpan_Ingredient.\n" +
                        "\t\t?mediSpan_Drug mapping:medispan_to_umls ?mediSpan_Ingredient.\n" +
                        "\t\t?mediSpan_Drug medispan:indication ?mediSpan_Disease.\n" +
                        "\t\t?mediSpan_Drug ?medispan ?mediSpan_Disease.}\n" +
                        "\n" +
                        "\tOPTIONAL{pharmgkb:" + pair.getDrug() + " pharmgkbv:x-umls ?mediSpan_Ingredient.\n" +
                        "\t\t?mediSpan_Drug mapping:medispan_to_umls ?mediSpan_Ingredient.\n" +
                        "\t\t?mediSpan_Drug medispan:side_effect ?mediSpan_Disease.\n" +
                        "\t\t?mediSpan_Drug ?medispan ?mediSpan_Disease.}\n" +
                        "\n" +
                        "\t##GP Link\n" +
                        "\tpharmgkb:" + pair.getGene() + " pharmgkbv:x-ncbigene ?clinvar_Gene.\n" +
                        "\t\n" +
                        "\t#ClinVar\n" +
                        "\t?gene_Variant clinvarv:x-gene ?clinvar_Gene.\n" +
                        "\t\t?clinvar_Variant clinvarv:Variant_Gene ?gene_Variant.\n" +
                        "\t\t?clinvar_Variant ?clinvarv ?clinvar_Disease.\n" +
                        "\n" +
                        "\t##Phenotype\n" +
                        "\t#ClinVar Medispan\n" +
                        "\t?clinvar_Disease mapping:clinvar_to_medispan ?mediSpan_Disease.\n" +
                        "}"
        );
    }


}

