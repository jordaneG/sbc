package sparql;

import model.Instance;
import model.Pair;
import org.apache.log4j.Logger;
import org.openrdf.OpenRDFException;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPTupleQuery;

import java.util.ArrayList;
import java.util.List;


public class Request extends Action {

    private static final Logger LOGGER = Logger.getLogger(Request.class);
    private List<Instance> matrix;

    public Request() {
        super();
        this.matrix = new ArrayList<>();
    }

    public List<Instance> getMatrix() {
        return matrix;
    }

    @Override
    public void execute(Pair pair) throws OpenRDFException {

        HTTPTupleQuery query = getQuery(getConnection(), pair);
        query.setMaxQueryTime(600000);

        TupleQueryResult result = query.evaluate();
        while (result.hasNext()) {
            BindingSet bindingSet = result.next();
            String[] gene_attributeCplt = (bindingSet.getValue("gene_attribute")).toString().split("/");
            String[] phenotypeCplt = bindingSet.getValue("phenotype").toString().split("/");
            String[] drug_attributeCplt = bindingSet.getValue("drug_attribute").toString().split("/");
            String[] gd_linkCplt = (bindingSet.getValue("gd_link").toString()).split("/");
            String[] gp_linkCplt = bindingSet.getValue("gp_link").toString().split("/");
            String[] dp_linkCplt = bindingSet.getValue("dp_link").toString().split("/");
            Instance instance = new Instance(pair.getGene() + "_" + pair.getDrug(), gene_attributeCplt[gene_attributeCplt.length - 1], phenotypeCplt[phenotypeCplt.length - 1], drug_attributeCplt[drug_attributeCplt.length - 1], gd_linkCplt[gd_linkCplt.length - 1], gp_linkCplt[gp_linkCplt.length - 1], dp_linkCplt[dp_linkCplt.length - 1], pair.getAssociation());

            matrix.add(instance);
        }
        result.close();
    }

    private HTTPTupleQuery getQuery(RepositoryConnection connection, Pair pair) throws MalformedQueryException, RepositoryException {
        return (HTTPTupleQuery) connection.prepareTupleQuery(QueryLanguage.SPARQL,
                "PREFIX atc: <http://bio2rdf.org/atc:>\n" +
                        "PREFIX bio2rdfv: <http://bio2rdf.org/bio2rdf_vocabulary:>\n" +
                        "PREFIX clinvar: <http://bio2rdf.org/clinvar:>\n" +
                        "PREFIX clinvarv: <http://bio2rdf.org/clinvar_vocabulary:>\n" +
                        "PREFIX dbv: <http://bio2rdf.org/drugbank_vocabulary:>\n" +
                        "PREFIX disgenet: <http://rdf.disgenet.org/resource/gda/>\n" +
                        "PREFIX drugbank: <http://bio2rdf.org/drugbank:>\n" +
                        "PREFIX mapping: <http://biodb.jp/mappings/>\n" +
                        "PREFIX medispan: <http://orpailleur.fr/medispan/>\n" +
                        "PREFIX ncbigene: <http://bio2rdf.org/ncbigene:>\n" +
                        "PREFIX pharmgkb: <http://bio2rdf.org/pharmgkb:>\n" +
                        "PREFIX pharmgkbv: <http://bio2rdf.org/pharmgkb_vocabulary:>\n" +
                        "PREFIX pubchemcompound: <http://bio2rdf.org/pubchem.compound:>\n" +
                        "PREFIX sider: <http://bio2rdf.org/sider:>\n" +
                        "PREFIX siderv: <http://bio2rdf.org/sider_vocabulary:>\n" +
                        "PREFIX sio: <http://semanticscience.org/resource/>\n" +
                        "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                        "PREFIX so: <http://bio2rdf.org/sequence_ontology:>\n" +
                        "PREFIX umls: <http://bio2rdf.org/umls:>\n" +
                        "PREFIX uniprot: <http://bio2rdf.org/uniprot:>\n" +
                        "\n" +
                        "\n" +
                        "SELECT DISTINCT ?gene_attribute ?phenotype ?drug_attribute ?gd_link ?gp_link ?dp_link\n" +
                        "WHERE {pharmgkb:" + pair.getDrug() + " pharmgkbv:x-drugbank ?drug2.\n" +
                        "       pharmgkb:" + pair.getDrug() + " pharmgkbv:x-umls ?cui.\n" +
                        "       pharmgkb:" + pair.getDrug() + " pharmgkbv:x-pubchemcompound ?compound.\n" +
                        "       ?drug3 siderv:pubchem-flat-compound-id ?compound. \n" +
                        "       ?drug_target dbv:drug ?drug2.\n" +
                        "       ?drug_target dbv:action ?gd_link.\n" +
                        "       ?drug2 dbv:x-pubchemcompound ?compound. \n" +
                        "       ?drug2 dbv:x-atc ?drug_attribute. \n" +
                        "       OPTIONAL {?drug_target dbv:enzyme ?target.}\n" +
                        " OPTIONAL {      ?drug_target dbv:target ?target.}\n" +
                        "    OPTIONAL {   ?drug_target dbv:transporter ?target.}\n" +
                        "     OPTIONAL {  ?drug_target dbv:carrier ?target.}\n" +
                        "       ?target dbv:x-uniprot ?prot.\n" +
                        "       pharmgkb:" + pair.getGene() + " pharmgkbv:x-uniprot ?prot.\n" +
                        "       pharmgkb:" + pair.getGene() + " pharmgkbv:x-ncbigene ?gene.\n" +
                        "       ?gene2 clinvarv:x-gene ?gene.\n" +
                        "       ?gene2 clinvarv:x-sequence_ontology ?gp_link.\n" +
                        "       ?rcv clinvarv:Variant_Gene ?gene2.\n" +
                        "       ?rcv clinvarv:Variant_Phenotype ?x. \n" +
                        "       ?x clinvarv:x-medgen ?disease2. \n" +
                        "       ?gene bio2rdfv:x-identifiers.org ?gene3.\n" +
                        "       ?var sio:SIO_000628 ?gene3.\n" +
                        "       ?var sio:SIO_000628 ?phenotype. \n" +
                        "       ?gene3 sio:SIO_000062 ?gene_attribute.\n" +
                        "       ?phenotype sio:SIO_000095 ?mesh.\n" +
                        "       ?phenotype sio:SIO_000008 ?semantic_type.\n" +
                        "       ?phenotype skos:exactMatch ?disease2. \n" +
                        "       ?drug3 ?dp_link ?disease3.\n" +
                        "       ?phenotype skos:exactMatch ?disease3. \n" +
                        "       ?disease4 mapping:medispan_to_sider ?disease3. \n" +
                        "       ?disease2 mapping:clinvar_to_sider ?disease3.\n" +
                        "       ?disease2 mapping:clinvar_to_medispan ?disease4.\n" +
                        "      }");
    }


}

