package sparql;

import model.Pair;
import org.openrdf.OpenRDFException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;

public abstract class Action {
    private static RepositoryConnection connection = null;

    public RepositoryConnection getConnection(){
        if (connection == null) {
            HTTPRepository httpRepository = new HTTPRepository("http://jordaneg.pro:9999/blazegraph/namespace/kb/sparql", "bigdata");
            try {
                httpRepository.initialize();
                connection = httpRepository.getConnection();
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public abstract void execute(Pair pair) throws OpenRDFException;

    public void close(){
        try {
            Repository repository = getConnection().getRepository();
            getConnection().close();
            repository.shutDown();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        connection = null;
    }

}
