package csv;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import model.Instance;
import model.Pair;
import org.apache.log4j.Logger;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import sparql.Action;

import java.io.*;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

public class CSVManager {
    private static Logger log = Logger.getLogger(CSVManager.class);

    public static void readTSV(Action request) {
        try {
            String line;
            BufferedReader br = new BufferedReader(new FileReader("src/main/resources/sparql/training_set_91_182.tsv"));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] data = line.split("\t");

                Pair pair = new Pair(data[0], data[1], data[2]);

                request.execute(pair);
            }
            br.close();
        } catch (Exception e) {
            log.debug(e);
        }
        request.close();
    }

    public static void toCSV(List<Instance> matrix) {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter("matrice.csv"), ',');
            String[] headers = "gene_attribute,phenotype,drug_attribute,gd_link,gp_link,dp_link,class".split(",");
            writer.writeNext(headers);

            for (Instance s : matrix) {
                writer.writeNext(s.toArray());
            }

            writer.close();
        } catch (IOException e) {
            log.debug(e);
        }
    }

    public static RecordReaderDataSetIterator getDataSetIteratorFromCSV(File csv, int classNumber, String delimiter, int numLinesToSkip, int labelIndex, int batchSize) {
        RecordReader recordReader = new CSVRecordReader(numLinesToSkip, delimiter);
        try {
            recordReader.initialize(new FileSplit(csv));
        } catch (IOException | InterruptedException e) {
            log.debug(e);
        }
        return new RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, classNumber);
    }

    public static File transformCSVForlearning(File csv) {

        CSVReader csvReader;
        CSVWriter writer;
        try {
            csvReader = new CSVReader(new FileReader(csv));
            FileWriter fileWriter = new FileWriter("src/resources/tmp.csv");
            writer = new CSVWriter(fileWriter, ',');
        } catch (IOException e) {
            log.debug(e);
            return null;
        }

        Iterator<String[]> iterator = csvReader.iterator();
        writer.writeNext(iterator.next());
        while (iterator.hasNext()) {
            String[] next = iterator.next();
            String[] encodeStrings = new String[next.length];
            for (int i = 0; i < next.length; i++) {
                encodeStrings[i] = stringToInt(next[i]).toString();
                log.info(next[i] + "--->" + encodeStrings[i]);
            }
            writer.writeNext(encodeStrings);
        }

        try {
            writer.close();
        } catch (IOException e) {
            log.debug(e);
        }

        return new File("src/resources/tmp.csv");
    }

    private static BigInteger stringToInt(String s) {
        try {
            int i = Integer.parseInt(s);
            return BigInteger.valueOf(i);
        } catch (NumberFormatException ignored) {
        }
        StringBuilder sb = new StringBuilder();
        for (char c : s.toCharArray())
            sb.append((int) c);

        return new BigInteger(sb.toString());
    }

}
