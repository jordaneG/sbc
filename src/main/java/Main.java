import csv.CSVManager;
import model.Instance;
import sparql.MedispanClinVar;
import sparql.MedispanDisGenet;
import sparql.SiderClinVar;
import sparql.SiderDisGenet;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Instance> matrix = new ArrayList<>();

        MedispanClinVar medispanClinVar = new MedispanClinVar();
        SiderDisGenet siderDisGenet = new SiderDisGenet();
        MedispanDisGenet medispanDisGenet = new MedispanDisGenet();
        SiderClinVar siderClinVar = new SiderClinVar();

        CSVManager.readTSV(medispanClinVar);
        CSVManager.readTSV(siderDisGenet);
        CSVManager.readTSV(medispanDisGenet);
        CSVManager.readTSV(siderClinVar);

        matrix.addAll(medispanClinVar.getMatrix());
        matrix.addAll(siderDisGenet.getMatrix());
        matrix.addAll(medispanDisGenet.getMatrix());
        matrix.addAll(siderClinVar.getMatrix());

        CSVManager.toCSV(matrix);
    }

}
