package model;

/**
 * Created by Yoann on 09/03/2017.
 */
public class Pair {

    private String gene;
    private String drug;
    private String association;

    public Pair(String gene, String drug, String association) {
        this.gene = gene;
        this.drug = drug;
        this.association = association;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "gene='" + gene + '\'' +
                ", drug='" + drug + '\'' +
                ", association='" + association + '\'' +
                '}';
    }

    public String getGene() {
        return gene;
    }

    public void setGene(String gene) {
        this.gene = gene;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }
}
