package model;

public class Instance {

    private String pair;
    private String geneAttribute;
    private String phenotype;
    private String drugAttribute;
    private String dgLink;
    private String gpLink;
    private String dpLink;
    private String association;

    public Instance(String pair, String geneAttribute, String drugAttribute, String dgLink, String association) {
        this.pair = pair;
        this.geneAttribute = geneAttribute;
        this.drugAttribute = drugAttribute;
        this.dgLink = dgLink;
        this.association = association;
    }

    public Instance(String pair, String geneAttribute, String phenotype, String drugAttribute, String dgLink, String gpLink, String dpLink, String association) {
        this.pair = pair;
        this.geneAttribute = geneAttribute;
        this.phenotype = phenotype;
        this.drugAttribute = drugAttribute;
        this.dgLink = dgLink;
        this.gpLink = gpLink;
        this.dpLink = dpLink;
        this.association = association;
    }


    public String[] toArray() {
        String ass ="";
        if (association.equals("associated")) {
            ass="1";
        } else {
            ass="0";
        }
        return new String[]{this.geneAttribute,this.phenotype,this.drugAttribute,this.dgLink,this.gpLink,this.dpLink,ass};
    }

    public void setGeneAttribute(String geneAttribute) {
        this.geneAttribute = geneAttribute;
    }

    public void setPhenotype(String phenotype) {
        this.phenotype = phenotype;
    }

    public void setDrugAttribute(String drugAttribute) {
        this.drugAttribute = drugAttribute;
    }

    public void setDgLink(String dgLink) {
        this.dgLink = dgLink;
    }

    public void setGpLink(String gpLink) {
        this.gpLink = gpLink;
    }

    public void setDpLink(String dpLink) {
        this.dpLink = dpLink;
    }

    public String getGeneAttribute() {
        return geneAttribute;
    }

    public String getPhenotype() {
        return phenotype;
    }

    public String getDrugAttribute() {
        return drugAttribute;
    }

    public String getDgLink() {
        return dgLink;
    }

    public String getGpLink() {
        return gpLink;
    }

    public String getDpLink() {
        return dpLink;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getPair() {
        return pair;
    }

    public String getAssociation() {
        return association;
    }

    @Override
    public String toString() {
        return "Instance{" +
                "pair='" + pair + '\'' +
                ", geneAttribute='" + geneAttribute + '\'' +
                ", phenotype='" + phenotype + '\'' +
                ", drugAttribute='" + drugAttribute + '\'' +
                ", dgLink='" + dgLink + '\'' +
                ", gpLink='" + gpLink + '\'' +
                ", dpLink='" + dpLink + '\'' +
                ", association='" + association + '\'' +
                '}';
    }
}
