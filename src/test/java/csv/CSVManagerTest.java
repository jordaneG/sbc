package csv;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;

class CSVManagerTest {
    @Test
    void getInstanceFromCSV() {
        File file = new File("src/test/java/csv/matrice.csv");
        Assertions.assertThat(CSVManager.transformCSVForlearning(file).isFile()).isTrue();
    }

}